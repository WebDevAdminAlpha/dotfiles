#!/usr/bin/python
# coding=utf-8

import os
import subprocess

from libqtile.config import Drag, Key, KeyChord, Click
from libqtile.command import lazy
from libqtile.config import ScratchPad, DropDown
from libqtile import extension
from groups import groups
from colors import *

# mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')
myTerm = "alacritty"

keys = [

# SUPER + FUNCTION KEYS

    Key([mod], "b", lazy.spawn('brave -no-default-browser-check')),
    Key([mod], "c", lazy.spawn(home + '/.scripts/term -e cava')),
    Key([mod], "d", lazy.spawn('discord')),
    Key([mod], "e", lazy.spawn(home + '/.scripts/term -e neomutt')),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "n", lazy.spawn(home + '/.scripts/nwrap -RDe')),
    Key([mod], "t", lazy.spawn('thunar')),
    Key([mod], "v", lazy.spawn("xfce4-terminal -e 'pulsemixer'")),
    Key([mod], "w", lazy.spawn('qutebrowser')),
    Key([mod], "x", lazy.spawn('arcolinux-logout')),
    Key([mod], "Escape", lazy.spawn('xkill')),
    Key([mod], "Return", lazy.spawn(myTerm)),
    Key([mod], "KP_Enter", lazy.spawn(myTerm)),
    Key([mod], "F5", lazy.spawn('meld')),
    Key([mod], "F7", lazy.spawn('virtualbox')),
    Key([mod], "F8", lazy.spawn('thunar')),
    Key([mod], "F11", lazy.spawn('rofi -show run -fullscreen')),
    Key([mod], "m", lazy.spawn(home + '/.scripts/term -e mocp')),

# SUPER + SHIFT KEYS

    Key([mod, "shift"], "Return", lazy.spawn("dmenu_run " + dm_theme + " -p '>'")),
    # Key([mod, "shift"], "q", lazy.shutdown()),
    Key([mod, "shift"], "r", lazy.restart()),
    Key([mod, "control"], "r", lazy.restart()),

# dmenu scripts launched with KeyChord SUPER+p followed by 'key'
    KeyChord([mod], "p", [
        Key([], "c",
            lazy.spawn(home + "/.scripts/dmenu/dconfig"),
            desc="Edit a config file"
            ),
        Key([], "s",
            lazy.spawn(home + "/.scripts/dmenu/dsys"),
            desc="Open a sysmon utility"
            ),
        Key([], "m",
            lazy.spawn(home + "/.scripts/dmenu/dmount"),
            desc="mount a drive"
            ),
        Key([], "u",
            lazy.spawn(home + "/.scripts/dmenu/dumount"),
            desc="unmount a drive"
            ),
        Key([], "w",
            lazy.spawn(home + "/.scripts/dmenu/dsearch"),
            desc="search the web"
            ),
        Key([], "p",
            lazy.spawn(home + "/.scripts/dmenu/dhandler"),
            desc="custom link handler"
            ),
        KeyChord([], "b", [
                Key([], "c",
                lazy.spawn("/usr/bin/btmenu"),
                    desc="connect to a bluetooth device"
                    ),
                Key([], "d",
                    lazy.spawn("/usr/bin/btmenu -d"),
                    desc="disconnect from a bluetooth device"
                    ),
                Key([], "v",
                    lazy.spawn(home + "/.scripts/bwrap"),
                    desc="open bitwarden vault in dmenu"
                    ),
        ]),
        Key([], "x",
            lazy.run_extension(extension.CommandSet(
            commands={
                'lock': 'betterlockscreen -l dimblur',
                'suspend': 'betterlockscreen -s dimblur',
                'logout': 'killall qtile',
                'hibernate': 'systemctl hibernate',
                'shutdown': 'systemctl poweroff',
                'reboot': 'systemctl reboot',
            },
            dmenu_prompt='session >',
            background=theme[1],
            foreground=theme[4],
            selected_background=theme[0],
            selected_foreground=theme[2],
            font='MesloLGS NF',
            fontsize=13,
        )), desc='List options to quit the session.')
    ]),

# rofi scripts launched by keychord SUPER+r and 'key'
    KeyChord([mod], "r", [
        Key([], "c",
            lazy.spawn(home + "/.scripts/rofi/rconfig"),
            desc="Edit a config file"
            ),
        Key([], "s",
            lazy.spawn(home + "/.scripts/rofi/rsys"),
            desc="Open a sysmon utility in the terminal"
            ),
        Key([], "w",
            lazy.spawn(home + "/.scripts/rofi/rsearch"),
            desc="search the web"
            ),
        Key([], "p",
            lazy.spawn(home + "/.scripts/rofi/rhandler"),
            desc="custom link handler"
            ),
        Key([], "x",
            lazy.spawn(home + "/.scripts/rofi/rlogout"),
            desc="List options to quit session"
            ),
        Key([], "b",
            lazy.spawn("/usr/bin/rofi-bluetooth"),
            desc="connect/disconnect to bluetooth devices"
            )
    ]),

# CONTROL + ALT KEYS

    Key(["mod1", "control"], "a", lazy.spawn('xfce4-appfinder')),
    Key(["mod1", "control"], "b", lazy.spawn('thunar')),
    Key(["mod1", "control"], "e", lazy.spawn('arcolinux-tweak-tool')),
    Key(["mod1", "control"], "g", lazy.spawn('brave -no-default-browser-check')),
    Key(["mod1", "control"], "h", lazy.spawn(home + '/.scripts/term -e htop')),
    Key(["mod1", "control"], "k", lazy.spawn('arcolinux-logout')),
    Key(["mod1", "control"], "l", lazy.spawn('betterlockscreen -l dimblur')),
    Key(["mod1", "control"], "s", lazy.spawn('betterlockscreen -s dimblur')),
    Key(["mod1", "control"], "m", lazy.spawn('xfce4-settings-manager')),
    Key(["mod1", "control"], "o", lazy.spawn(home + '/.config/qtile/scripts/picom-toggle.sh')),
    Key(["mod1", "control"], "p", lazy.spawn('pamac-manager')),
    Key(["mod1", "control"], "r", lazy.spawn('rofi-theme-selector')),
    Key(["mod1", "control"], "t", lazy.spawn(myTerm)),
    Key(["mod1", "control"], "u", lazy.spawn('pavucontrol')),
    Key(["mod1", "control"], "w", lazy.spawn('arcolinux-welcome-app')),
    Key(["mod1", "control"], "Return", lazy.spawn('rofi -show run')),

# ALT + ... KEYS

    Key(["mod1"], "e", lazy.spawn(home + '/.scripts/term xfce4-terminal -e neomutt --geometry=120x40')),
    Key(["mod1"], "h", lazy.spawn(home + '/.scripts/term xfce4-terminal -e htop --geometry=120x40')),
    Key(["mod1"], "m", lazy.spawn(home + '/.scripts/term xfce4-terminal -e mocp --geometry=120x40')),
    Key(["mod1"], "n", lazy.spawn("mocp -f")),
    Key(["mod1"], "b", lazy.spawn("mocp -r")),
    Key(["mod1"], "p", lazy.spawn("mocp -G")),
    Key(["mod1"], "s", lazy.spawn("mocp -s")),
    Key(["mod1"], "c", lazy.spawn(home + '/.scripts/term xfce4-terminal -e cava --geometry=120x40')),
    Key(["mod1"], "F2", lazy.spawn('gmrun')),
    Key(["mod1"], "F3", lazy.spawn('xfce4-appfinder')),

# CONTROL + SHIFT KEYS

    Key([mod2, "shift"], "Escape", lazy.spawn('xfce4-taskmanager')),
    Key([mod2, "shift"], "t", lazy.spawn('brave --profile-directory=Default --app-id=hpfldicfbfomlpcikngkocigghgafkph')), #Messages
    Key([mod2, "shift"], "e", lazy.spawn('brave --profile-directory=Default --app-id=kmhopmchchfpfdcdjodmpfaaphdclmlj')), #Gmail
    Key([mod2, "shift"], "d", lazy.spawn('brave --profile-directory=Default --app-id=imgohncinckhbblnlmaedahepnnpmdma')), #Google Duo
    Key([mod2, "shift"], "h", lazy.spawn('brave --profile-directory=Default --app-id=odadmohlkalmmfdgjdlbjdpoekbijhcc')), #Google Hangouts
    Key([mod2, "shift"], "p", lazy.spawn('plexmediaplayer')), #Plex
    Key([mod2, "shift"], "m", lazy.spawn("Plexamp.AppImage")), #Plexamp
    # Key([mod2, "shift"], "y", lazy.spawn('brave --profile-directory=Default --app-id=cinhimbnkkaeohfgghhklpknlkffjgod')), #YouTube Music
    Key([mod2, "shift"], "y", lazy.spawn("youtube-music-bin")), # YouTube Music standalone

# SCREENSHOTS

    Key([], "Print", lazy.spawn("scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")),
    Key([mod2], "Print", lazy.spawn('xfce4-screenshooter')),
    Key([mod2, "shift"], "Print", lazy.spawn('gnome-screenshot -i')),

# QTILE LAYOUT KEYS
    # Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),

# CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),


# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        # lazy.layout.grow(),
        # lazy.layout.increase_ratio(),
        # lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        # lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        # lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        # lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),

# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
    Key([mod], "z", lazy.window.toggle_minimize()),

# TOGGLE SPLIT MODE IN COLUMNS LAYOUT
    Key([mod], "semicolon", lazy.layout.toggle_split()),]

for i in groups:
    keys.extend([

# CHANGE WORKSPACES
    Key([mod], i.name, lazy.group[i.name].toscreen()),
    Key([mod], "Tab", lazy.screen.toggle_group()),
    Key(["mod1"], "Tab", lazy.screen.toggle_group()),
    Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

# ScratchPad
groups.append(
    ScratchPad("scratchpad", [
        DropDown("termtop", "/usr/bin/alacritty --config-file /home/chris/.config/alacritty/alacritty-dropdown.yml", opacity=1.0, height=0.55, width=0.80,),
        DropDown("termleft", "/usr/bin/alacritty --config-file /home/chris/.config/alacritty/alacritty-dropdown.yml", opacity=1.0, height=0.9, width=0.33, x=0.001, y=0.05,),
        DropDown("termright", "/usr/bin/alacritty --config-file /home/chris/.config/alacritty/alacritty-dropdown.yml", opacity=1.0, height=0.9, width=0.33, x=0.67, y=0.05,),
        DropDown("termbottom", "/usr/bin/alacritty --config-file /home/chris/.config/alacritty/alacritty-dropdown.yml", opacity=1.0, height=0.55, width=0.80, y=0.45),
    ]),
)

keys.extend([
    KeyChord([mod], 's', [
        Key([], 'k', lazy.group['scratchpad'].dropdown_toggle('termtop')),
        Key([], 'h', lazy.group['scratchpad'].dropdown_toggle('termleft')),
        Key([], 'l', lazy.group['scratchpad'].dropdown_toggle('termright')),
        Key([], 'j', lazy.group['scratchpad'].dropdown_toggle('termbottom')),
    ]),
])

# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]
