# Qtile config
# by Chris Noseworthy
# (https://gitlab.com/cnose)
#
# modified from the ArcoLinux Qtile config
# (https://arcolinux.info)
#
#
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import os
# import re
# import socket
import subprocess
from libqtile import qtile
# from libqtile.config import Drag, Key, KeyChord, Group, Match, Click, Rule
from libqtile.config import Match
from libqtile import hook
from libqtile.command import lazy
from typing import List # noqa: F401
from groups import groups
from keys import keys, mouse
from widgets import screens
from layouts import layout_theme, layouts, floating_layout

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
@hook.subscribe.client_new
def assign_app_group(client):
     d = {}
     #########################################################
     ################ assign apps to groups ##################
     #########################################################
     d["1"] = []
     d["2"] = [ "brave-browser", "qutebrowser", ] #Web Browsers
     d["3"] = [ "crx_kmhopmchchfpfdcdjodmpfaaphdclmlj",  ] #GMail
     d["4"] = [ "crx_hpfldicfbfomlpcikngkocigghgafkph", "signal", "crx_odadmohlkalmmfdgjdlbjdpoekbijhcc", "crx_imgohncinckhbblnlmaedahepnnpmdma", "discord", ]
            #Android Messages,WhatsApp,Google Duo, Hangouts, Discord
     d["5"] = [ "plexmediaplayer", ]
     d["6"] = [ "", ] #steam
     d["7"] = [ "youtube music", "crx_cinhimbnkkaeohfgghhklpknlkffjgod", "plexamp", ] #Youtube Music, Plexamp
     d["8"] = [ "VirtualBox Manager", ] #Virtualbox
     d["9"] = [ "thunar", ]
     d["0"] = [ "org.gnome.meld", ]
     ##########################################################
     wm_class = client.window.get_wm_class()[0]

     for i in range(len(d)):
         if wm_class in list(d.values())[i]:
             group = list(d.keys())[i]
             client.togroup(group)
             client.group.cmd_toscreen(toggle=False)

main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]

follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True

focus_on_window_activation = "smart" # or focus

wmname = "LG3D"
