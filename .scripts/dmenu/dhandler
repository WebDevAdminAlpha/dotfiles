#!/bin/sh

# This script will paste the contents of your clipboard to dmenu and then you
# choose what program to use to open it
#
# Dependencies: dmenu, youtube-dl, mpv, moc, kdeconnect-cli, task spooler

notify_start () {
    notify-send -u low "$summary" "Your $name is downloading..." --icon="$icon"
}
notify_done () {
    notify-send -u low "$summary" "Your $name is ready!" --icon="$icon" && paplay "/usr/share/sounds/deepin/stereo/audio-volume-change.wav"
}
notify_error () {
    notify-send -u critical "OH NO" "What'd ya do?!" --icon=face-worried
}
dm="dmenu -i -h 24 -nb #2F3440 -nf #81A1C1 -sb #4C566A -sf #C0C5CE" # custom dmenu options
font="MesloLGS NF" # dmenu font
clip=$(xclip -selection clipboard -o) # current clipboard contents
browser="qutebrowser" # I can't get it to read my $BROWSER env variable otherwise

case "$(printf "mpv\\nphone\\nmoc\\ndownload\\nytdl-(vid)\\nytdl-(alb)\\nytdl-(song)\\nbrowser" | $dm -fn "$font" -p "Send $clip to >")" in
    # stream video in mpv
	"mpv") notify-send -u low "MPV" "One moment please..." --icon=video-x-generic ;
        setsid -f mpv -quiet "$clip" >/dev/null 2>&1 ||
        notify_error ;; 
    # send link to phone's clipboard via kdeconnect
    "phone") notify-send -u low "kdeconnect" " Sending link to your phone's clipboard..." ;
        setsid -f kdeconnect-cli --share-text "$clip" -n OP8T >/dev/null 2>&1 ;;
    # stream Youtube Music in mocp    
	"moc") /home/chris/.scripts/mocplay "$clip" >/dev/null 2>&1 ;;
    # download generic files
	"download") summary=Files name=file icon=document-save-as ;
        tsp -f curl -LO --output-dir ~/Downloads "$clip" >/dev/null 2>&1 ||
        notify_error ;;
    # download YouTube videos
    "ytdl-(vid)") summary=YT-DL name=video icon=video-x-generic notify_start &&
        tsp -f youtube-dl --add-metadata -ic -o "$HOME/Videos/%(title)s.%(ext)s" "$clip" -f mp4 >/dev/null 2>&1 &&
        summary=YT-DL name=video icon=video-x-generic notify_done ||
        notify_error ;;
    # download YouTube Music full album
    "ytdl-(alb)") summary=YT-DL name=album icon=audio-x-generic notify_start &&
        tsp -f youtube-dl --add-metadata -ic -o "$HOME/Music/%(artist)s/%(album)s/%(playlist_index)02d-%(title)s.%(ext)s" "$clip" -x --audio-format mp3 --audio-quality 0 >/dev/null 2>&1 &&
        summary="YT-DL" name=album icon=audio-x-generic notify_done ||
        notify_error ;;
    # download single YouTube Music track
    "ytdl-(song)") summary="YT-DL" name=song icon=audio-x-generic notify_start &&
        echo "$clip" | cut -d "&" -f 1 | xargs tsp -f youtube-dl --add-metadata -ic -o "$HOME/Music/%(artist)s-%(title)s.%(ext)s" -x --audio-format mp3 --audio-quality 0 >/dev/null 2>&1 &&
        summary="YT-DL" name=song icon=audio-x-generic notify_done ||
        notify_error ;;
    # open link in web browser
	"browser") "$browser" "$clip" >/dev/null 2>&1 ;;
    *) exit ;;
esac
