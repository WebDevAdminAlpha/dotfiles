#!/usr/bin/python
# coding=utf-8

import os
import subprocess
from libqtile import widget, bar, qtile
from libqtile.config import Screen
from libqtile.widget import Spacer
from laptopbattery import LaptopBatteryWidget
from colors import theme

home = os.path.expanduser('~')

def get_weather():
    return subprocess.check_output(['/usr/bin/curl', 'wttr.in/Riondel?format=2']).decode('utf-8').strip()

def check_mail():
    return subprocess.check_output(['/home/chris/.scripts/mailcheck']).decode('utf-8').strip()

# WIDGETS FOR THE BAR

widget_defaults = dict(
    # font="Noto Sans",
    # fontsize = 12,
    # padding = 2,
    background = theme[1],
)

sep_defaults = dict(
    linewidth = 1,
    padding = 10,
    foreground = theme[2],
    background = theme[1],
)

screens = [
    Screen(
        top=bar.Bar(
            [
               widget.GroupBox(
                        **widget_defaults,
                        font="FontAwesome",
                        fontsize = 16,
                        margin_y = 2,
                        margin_x = 0,
                        padding_y = 6,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = theme[2],
                        inactive = theme[0],
                        rounded = False,
                        highlight_method = "text",
                        this_current_screen_border = theme[8],
                        foreground = theme[2]
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.CurrentLayoutIcon(
                        **widget_defaults,
                        custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons/layouts")],
                        foreground = theme[5],
                        padding = 0,
                        scale = 0.5
                        ),
               widget.CurrentLayout(
                        **widget_defaults,
                        font = "Noto Sans Bold",
                        foreground = theme[5],
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.WindowName(font="Noto Sans",
                        **widget_defaults,
                        fontsize = 12,
                        foreground = theme[5],
                        ),
                widget.Net(
                         **widget_defaults,
                         font="Noto Sans",
                         fontsize = 12,
                         interface = "wlp3s0",
                         foreground = theme[5],
                         padding = 2,
                         format='{down} ↓↑ {up}'
                         ),
                widget.Sep(
                         **sep_defaults,
                         ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[6],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('/home/chris/.scripts/cpu')}
                        ),
               widget.CPUGraph(
                        **widget_defaults,
                        border_color = theme[2],
                        fill_color = theme[7],
                        graph_color = theme[8],
                        border_width = 1,
                        line_width = 1,
                        core = "all",
                        type = "box",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('xfce4-terminal -e bpytop --geometry=120x45')}
                        ),
               widget.ThermalSensor(
                        **widget_defaults,
                        foreground = theme[2],
                        foreground_alert = theme[6],
                        font = "Noto Sans",
                        fontsize = 12,
                        threshold = 90,
                        update_interval = 5
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[4],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('/home/chris/.scripts/mem')}
                        ),
               widget.Memory(
                        **widget_defaults,
                        font="Noto Sans",
                        format = '{MemUsed}M/{MemTotal}M',
                        update_interval = 1,
                        fontsize = 12,
                        foreground = theme[5],
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('/usr/bin/xfce4-terminal -e bpytop --geometry=120x45')}
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.Moc(
                        **widget_defaults,
                        foreground = theme[5],
                        font = "Noto Sans",
                        fontsize = 12,
                        noplay_color = theme[0],
                        padding = 0,
                        play_color = theme[5],
                        update_interval= 1,
                        max_chars= 50
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[3],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(home + '/.scripts/term xfce4-terminal -e "calcurse" --geometry=120x45')}
                        ),
               widget.Clock(
                        **widget_defaults,
                        foreground = theme[5],
                        font = "Noto Sans",
                        fontsize = 12,
                        format="%a %h %-d %-I:%M %p",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(home + '/.scripts/term xfce4-terminal -e "khal calendar" -H')}
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[7],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks =
                            {'Button1': lambda: qtile.cmd_spawn(home + '/.scripts/term xfce4-terminal -e neomutt --geometry=125x35'),
                             'Button3': lambda: qtile.cmd_spawn(home + '/.scripts/mailcheck')}
                        ),
               widget.GenPollText(
                        **widget_defaults,
                        font= "Noto Sans",
                        fontsize = 14,
                        foreground = theme[5],
                        func = check_mail,
                        update_interval = 120
                        ),
               # widget.Maildir(
               #          font = "Noto Sans",
               #          fontsize = 14,
               #          maildir_path = "~/.local/share/mail/gmail",
               #          sub_folders = [{"path": "INBOX", "label":""}],
               #          total = False,
               #          update_interval = 20,
               #          foreground = theme[5]
               #          ),
               widget.Sep(
                        **sep_defaults,
                        ),
               # widget.Wttr(
               #          foreground = theme[9],
               #          font = "Noto Sans",
               #          fontsize = 12,
               #          format = '2',
               #          location = 'Riondel',
               #          units = 'm',
               #          update_interval = 600
               #          ),
               widget.GenPollText(
                        **widget_defaults,
                        font = "Noto Sans",
                        fontsize = 12,
                        foreground = theme[5],
                        func = get_weather,
                        update_interval = 600,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('xfce4-terminal -e "curl wttr.in/Riondel?m" --geometry=125x45 -H')}
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               LaptopBatteryWidget(
                        **widget_defaults,
                        font = "Noto Sans",
                        fontsize = 12,
                        border_charge_colour = theme[11],
                        border_colour = theme[9],
                        border_critical_colour = theme[6],
                        fill_normal = theme[9],
                        fill_low = theme[10],
                        fill_critical = theme[6],
                        percentage_low = 0.30,
                        percentage_critical = 0.15,
                        text_charging = "({percentage:.0f}%) ttf: {ttf}",
                        text_discharging = "({percentage:.0f}%) tte: {tte}",
                        text_displaytime = 2
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.Systray(
                        **widget_defaults,
                        icon_size = 20,
                        padding = 4
                        ),
              ],
              26,
          ),
      ),
  ]
