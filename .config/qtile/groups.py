#!/usr/bin/python
# coding=utf-8
from libqtile.config import Group

groups = []

# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

#group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]
group_labels = ["", "", "", "", "", "", "", "", "", "",]
#group_labels = ["term", "www", "email", "chat", "plex", "steam", "music", "vbox", "files", "misc",]

group_layouts = ["columns", "max", "max", "matrix", "max", "max", "monadtall", "floating", "bsp", "max",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))
