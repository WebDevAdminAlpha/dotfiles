#!/usr/bin/python
# coding=utf-8

# Nord theme
theme = (
    '#4C566A',
    '#2E3440',
    '#C0C5CE',
    '#EBCB8B',
    '#81A1C1',
    '#ECEFF4',
    '#BF616A',
    '#B48EAD',
    '#6790EB',
    '#D8DEE9',
    '#d08770',
    '#a3be8c',
)

dm_theme = (
    "-i -h 24 -nb #2F3440 -nf #81A1C1 -sb #4C566A -sf #c0c5ce -fn 'MesloLGS NF'"
)
