#!/usr/bin/python
# code=utf-8

from libqtile import layout
from libqtile.config import Match
from colors import theme

def init_layout_theme():
    return {"margin":5,
            "border_width":2,
            "border_focus": theme[4],
            "border_normal": theme[1]
            }

layout_theme = init_layout_theme()

layouts = [
    layout.Columns(**layout_theme, num_columns=2, split=True, border_focus_stack=theme[4], border_normal_stack=theme[1], border_on_single=True),
    layout.MonadTall(margin=8, border_width=2, border_focus=theme[4], border_normal=theme[1]),
    layout.MonadWide(margin=8, border_width=2, border_focus=theme[4], border_normal=theme[1]),
    layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme)
]

floating_layout = layout.Floating(float_rules=[
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='confirmreset'),
    Match(wm_class='makebranch'),
    Match(wm_class='maketag'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),
    Match(wm_class='yad'),
    Match(title='branchdialog'),
    # Match(title='Open File'),
    Match(title='Unlock Login Keyring'),
    Match(title='pinentry'),
    Match(wm_class='ssh-askpass'),
    Match(wm_class='VirtualBox Manager'),
    Match(wm_class='redshift-gtk'),
    Match(wm_class='blueberry.py'),

],  fullscreen_border_width = 0, border_width = 0)
