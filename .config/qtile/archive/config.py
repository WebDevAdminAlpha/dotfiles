# Qtile config
# by Chris Noseworthy
# (https://gitlab.com/cnose)
#
# modified from the ArcoLinux Qtile config
# (https://arcolinux.info)
#
#
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Drag, Key, KeyChord, Screen, Group, Match, Click, Rule
from libqtile.config import ScratchPad, DropDown
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook, extension
from libqtile.widget import Spacer
from libqtile.lazy import lazy
from typing import List # noqa: F401

#mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')
myTerm = "termite"

def get_weather():
    return subprocess.check_output(['/usr/bin/curl', 'wttr.in/Riondel?m&format=2']).decode('utf-8').strip()

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

# COLORSCHEME

MYCOLORS = [
    '#4C566A',
    '#2E3440',
    '#C0C5CE',
    '#EBCB8B',
    '#81A1C1',
    '#ECEFF4',
    '#BF616A',
    '#B48EAD',
    '#6790EB',
    '#D8DEE9'
]

POLAR = MYCOLORS[0]
BLACK = MYCOLORS[1]
GREY = MYCOLORS[2]
YELLOW = MYCOLORS[3]
ICEBLUE = MYCOLORS[4]
WHITE = MYCOLORS[5]
RED = MYCOLORS[6]
MAGENTA = MYCOLORS[7]
BLUE = MYCOLORS[8]
SILVER = MYCOLORS[9]

keys = [

# FUNCTION KEYS

    # Key([], "F12", lazy.spawn('xfce4-terminal --drop-down')),

# SUPER + FUNCTION KEYS

    Key([mod], "b", lazy.spawn('brave -no-default-browser-check')),
    Key([mod], "d", lazy.spawn('discord')),
    Key([mod], "e", lazy.spawn(home + '/.scripts/term termite -e neomutt')),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "n", lazy.spawn(home + '/.scripts/nwrap -RD')),
    Key([mod], "t", lazy.spawn('thunar')),
    Key([mod], "v", lazy.spawn("xfce4-terminal -e 'pulsemixer'")),
    Key([mod], "x", lazy.spawn('arcolinux-logout')),
    Key([mod], "Escape", lazy.spawn('xkill')),
    Key([mod], "Return", lazy.spawn(myTerm)),
    Key([mod], "KP_Enter", lazy.spawn(myTerm)),
    Key([mod], "F5", lazy.spawn('meld')),
    Key([mod], "F7", lazy.spawn('virtualbox')),
    Key([mod], "F8", lazy.spawn('thunar')),
    Key([mod], "F11", lazy.spawn('rofi -show run -fullscreen')),
    Key([mod], "m", lazy.spawn('rofi -show run')),

# SUPER + SHIFT KEYS

    Key([mod, "shift"], "Return", lazy.spawn("dmenu_run -i -h 24 -nb '#2F3440' -nf '#81A1C1' -sb '#4C566A' -sf '#c0c5ce' -fn 'MesloLGS NF:regular:pixelsize=16' -p '>'")),
    Key([mod, "shift"], "d", lazy.spawn("dmenu_run -i -h 24 -nb '#2F3440' -nf '#81A1C1' -sb '#4C566A' -sf '#c0c5ce' -fn 'MesloLGS NF:regular:pixelsize=14' -p '>'")),
    Key([mod, "shift"], "q", lazy.shutdown()),
    Key([mod, "shift"], "r", lazy.restart()),
    Key([mod, "control"], "r", lazy.restart()),

# dmenu scripts launched with KeyChord SUPER+p followed by 'key'
    KeyChord([mod], "p", [
        Key([], "c",
            lazy.spawn(home + "/.scripts/dmenu/dconfig"),
            desc="Edit a config file"
            ),
        Key([], "s",
            lazy.spawn(home + "/.scripts/dmenu/dsys"),
            desc="Open a sysmon utility"
            ),
        Key([], "m",
            lazy.spawn(home + "/.scripts/dmenu/dmount"),
            desc="mount a drive"
            ),
        Key([], "u",
            lazy.spawn(home + "/.scripts/dmenu/dumount"),
            desc="unmount a drive"
            ),
        Key([], "w",
            lazy.spawn(home + "/.scripts/dmenu/dsearch"),
            desc="search the web"
            ),
        Key([], "p",
            lazy.spawn(home + "/.scripts/dmenu/dhandler"),
            desc="custom link handler"
            ),
        KeyChord([], "b", [
                Key([], "c",
                lazy.spawn("/usr/bin/btmenu"),
                    desc="connect to a bluetooth device"
                    ),
                Key([], "d",
                    lazy.spawn("/usr/bin/btmenu -d"),
                    desc="disconnect from a bluetooth device"
                    ),
        ]),
        Key([], "x",
            lazy.run_extension(extension.CommandSet(
            commands={
                'lock': 'betterlockscreen -l dimblur',
                'suspend': 'betterlockscreen -s dimblur',
                'hibernate': 'systemctl hibernate',
                'shutdown': 'systemctl poweroff',
                'reboot': 'systemctl reboot',
            },
            dmenu_prompt='session >',
            background='#2F3440',
            foreground='#5E81AC',
            selected_background='#4C566A',
            selected_foreground='#C0C5CE',
            font='MesloLGS NF',
            fontsize=13,
        )), desc='List options to quit the session.')
    ]),

# rofi scripts launched by keychord SUPER+r and 'key'
    KeyChord([mod], "r", [
        Key([], "c",
            lazy.spawn(home + "/.scripts/rofi/rconfig"),
            desc="Edit a config file"
            ),
        Key([], "s",
            lazy.spawn(home + "/.scripts/rofi/rsys"),
            desc="Open a sysmon utility in the terminal"
            ),
        Key([], "w",
            lazy.spawn(home + "/.scripts/rofi/rsearch"),
            desc="search the web"
            ),
        Key([], "p",
            lazy.spawn(home + "/.scripts/rofi/rhandler"),
            desc="custom link handler"
            ),
        Key([], "x",
            lazy.spawn(home + "/.scripts/rofi/rlogout"),
            desc="List options to quit session"
            )
    ]),
# CONTROL + ALT KEYS

    Key(["mod1", "control"], "a", lazy.spawn('xfce4-appfinder')),
    Key(["mod1", "control"], "b", lazy.spawn('thunar')),
    Key(["mod1", "control"], "e", lazy.spawn('arcolinux-tweak-tool')),
    Key(["mod1", "control"], "g", lazy.spawn('brave -no-default-browser-check')),
    Key(["mod1", "control"], "k", lazy.spawn('arcolinux-logout')),
    Key(["mod1", "control"], "l", lazy.spawn('betterlockscreen -l dimblur')),
    Key(["mod1", "control"], "s", lazy.spawn('betterlockscreen -s dimblur')),
    Key(["mod1", "control"], "m", lazy.spawn('xfce4-settings-manager')),
    Key(["mod1", "control"], "o", lazy.spawn(home + '/.config/qtile/scripts/picom-toggle.sh')),
    Key(["mod1", "control"], "p", lazy.spawn('pamac-manager')),
    Key(["mod1", "control"], "r", lazy.spawn('rofi-theme-selector')),
    Key(["mod1", "control"], "t", lazy.spawn(myTerm)),
    Key(["mod1", "control"], "u", lazy.spawn('pavucontrol')),
    Key(["mod1", "control"], "w", lazy.spawn('arcolinux-welcome-app')),
    Key(["mod1", "control"], "Return", lazy.spawn(myTerm)),

# ALT + ... KEYS

    Key(["mod1"], "h", lazy.spawn(home + '/.scripts/term termite -e htop')),
    Key(["mod1"], "m", lazy.spawn(home + '/.scripts/term termite -e mocp')),
    Key(["mod1"], "F2", lazy.spawn('gmrun')),
    Key(["mod1"], "F3", lazy.spawn('xfce4-appfinder')),

# CONTROL + SHIFT KEYS

    Key([mod2, "shift"], "Escape", lazy.spawn('xfce4-taskmanager')),
    Key([mod2, "shift"], "t", lazy.spawn('brave --profile-directory=Default --app-id=hpfldicfbfomlpcikngkocigghgafkph')), #Messages
    Key([mod2, "shift"], "e", lazy.spawn('brave --profile-directory=Default --app-id=kmhopmchchfpfdcdjodmpfaaphdclmlj')), #Gmail
    Key([mod2, "shift"], "d", lazy.spawn('brave --profile-directory=Default --app-id=imgohncinckhbblnlmaedahepnnpmdma')), #Google Duo
    Key([mod2, "shift"], "h", lazy.spawn('brave --profile-directory=Default --app-id=odadmohlkalmmfdgjdlbjdpoekbijhcc')), #Google Hangouts
    Key([mod2, "shift"], "p", lazy.spawn('plexmediaplayer')), #Plex
    Key([mod2, "shift"], "m", lazy.spawn("Plexamp.AppImage")), #Plexamp
    Key([mod2, "shift"], "y", lazy.spawn('brave --profile-directory=Default --app-id=cinhimbnkkaeohfgghhklpknlkffjgod')), #YouTube Music

# SCREENSHOTS

    Key([], "Print", lazy.spawn("scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")),
    Key([mod2], "Print", lazy.spawn('xfce4-screenshooter')),
    Key([mod2, "shift"], "Print", lazy.spawn('gnome-screenshot -i')),

# MULTIMEDIA KEYS

# INCREASE/DECREASE BRIGHTNESS
    # Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 10")),
    # Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 10")),

# INCREASE/DECREASE/MUTE VOLUME
    # Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    # Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    # Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),

    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),

# QTILE LAYOUT KEYS
    # Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),

# CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),


# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),


# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
    Key([mod], "z", lazy.window.toggle_minimize()),]

# Bring floating window to front

    # Key([mod], "grave", lazy.window.bring_to_front()),]

groups = []

# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

#group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]
group_labels = ["", "", "", "", "", "", "", "", "", "",]
#group_labels = ["term", "www", "email", "chat", "plex", "steam", "music", "vbox", "files", "misc",]

group_layouts = ["monadwide", "max", "max", "matrix", "max", "max", "monadtall", "floating", "bsp", "max",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod],
            i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.toggle_group()),
        Key(["mod1"], "Tab", lazy.screen.toggle_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

groups.append(
    ScratchPad("scratchpad", [
        DropDown("term", "/usr/bin/termite -c /home/chris/.config/termite/dropdown-config", opacity=1.0, height=0.55, width=0.80,),
    ]), )

keys.extend([
    Key([], 'F12', lazy.group['scratchpad'].dropdown_toggle('term')),
])

def init_layout_theme():
    return {"margin":5,
            "border_width":2,
            "border_focus": ICEBLUE,
            "border_normal": BLACK
            }

layout_theme = init_layout_theme()


layouts = [
    layout.MonadTall(margin=8, border_width=2, border_focus=ICEBLUE, border_normal=BLACK),
    layout.MonadWide(margin=8, border_width=2, border_focus=ICEBLUE, border_normal=BLACK),
    layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme)
]


# WIDGETS FOR THE BAR

def init_widgets_defaults():
    return dict(font="Noto Sans",
                fontsize = 12,
                padding = 2,
                background = BLACK)

widget_defaults = init_widgets_defaults()

def init_widgets_list():
    widgets_list = [
               widget.GroupBox(font="FontAwesome",
                        fontsize = 16,
                        margin_y = 2,
                        margin_x = 0,
                        padding_y = 6,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = GREY,
                        inactive = POLAR,
                        rounded = False,
                        highlight_method = "text",
                        this_current_screen_border = BLUE,
                        foreground = GREY,
                        background = BLACK
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = GREY,
                        background = BLACK
                        ),
               widget.CurrentLayoutIcon(
                        custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons/layouts")],
                        foreground = WHITE,
                        background = BLACK,
                        padding = 0,
                        scale = 0.5
                        ),
               widget.CurrentLayout(
                        font = "Noto Sans Bold",
                        foreground = WHITE,
                        background = BLACK
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = GREY,
                        background = BLACK
                        ),
               widget.WindowName(font="Noto Sans",
                        fontsize = 12,
                        foreground = WHITE,
                        background = BLACK
                        ),
                widget.Net(
                         font="Noto Sans",
                         fontsize = 12,
                         interface = "enp37s0",
                         foreground = WHITE,
                         background = BLACK,
                         padding = 2,
                         format='{down} ↓↑ {up}'
                         ),
                widget.Sep(
                         linewidth = 1,
                         padding = 10,
                         foreground = GREY,
                         background = BLACK
                         ),
               widget.TextBox(
                        font = "FontAwesome",
                        text = "  ",
                        foreground = RED,
                        background = BLACK,
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('/home/chris/.scripts/cpu')}
                        ),
               widget.CPUGraph(
                        border_color = GREY,
                        fill_color = BLUE,
                        graph_color = POLAR,
                        background = BLACK,
                        border_width = 1,
                        line_width = 1,
                        core = "all",
                        type = "box",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('xfce4-terminal -e bpytop --geometry=120x45')}
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = GREY,
                        background = BLACK
                        ),
               widget.TextBox(
                        font = "FontAwesome",
                        text = "  ",
                        foreground = ICEBLUE,
                        background = BLACK,
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('/home/chris/.scripts/mem')}
                        ),
               widget.Memory(
                        font="Noto Sans",
                        format = '{MemUsed}M/{MemTotal}M',
                        update_interval = 1,
                        fontsize = 12,
                        foreground = WHITE,
                        background = BLACK,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('/usr/bin/xfce4-terminal -e bpytop --geometry=120x45')}
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = GREY,
                        background = BLACK
                        ),
               widget.TextBox(
                        font = "FontAwesome",
                        text = "  ",
                        foreground = YELLOW,
                        background = BLACK,
                        padding = 0,
                        fontsize = 16
                        ),
               widget.Clock(
                        foreground = WHITE,
                        background = BLACK,
                        fontsize = 12,
                        format="%a %h %-d %-I:%M %p",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('xfce4-terminal -e "khal calendar" -H')}
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = GREY,
                        background = BLACK
                        ),
               widget.GenPollText(
                        font = "Noto Sans",
                        fontsize = 12,
                        foreground = WHITE,
                        background = BLACK,
                        func = get_weather,
                        update_interval = 600,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('xfce4-terminal -e "curl wttr.in/Riondel?m" --geometry=125x45 -H')}
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = GREY,
                        background = BLACK
                        ),
               widget.Systray(
                        background = BLACK,
                        icon_size = 20,
                        padding = 4
                        ),
              ]
    return widgets_list

widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26))]
screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

@hook.subscribe.client_new
def assign_app_group(client):
     d = {}
     #########################################################
     ################ assign apps to groups ##################
     #########################################################
     d["1"] = []
     d["2"] = [ "brave-browser", ]
     d["3"] = [ "crx_kmhopmchchfpfdcdjodmpfaaphdclmlj", ] #GMail
     d["4"] = [ "crx_hpfldicfbfomlpcikngkocigghgafkph", "whatsapp-nativefier-d40211", "crx_odadmohlkalmmfdgjdlbjdpoekbijhcc", "crx_imgohncinckhbblnlmaedahepnnpmdma", "discord", ]
            #Android Messages,WhatsApp,Google Duo, Hangouts, Discord
     d["5"] = [ "plexmediaplayer", ]
     d["6"] = [ "", ] #steam
     d["7"] = [ "crx_cinhimbnkkaeohfgghhklpknlkffjgod", "plexamp", ] #Youtube Music, Plexamp
     d["8"] = [ "VirtualBox Manager", ] #Virtualbox
     d["9"] = [ "thunar", ]
     d["0"] = [ "org.gnome.meld", ]
     ##########################################################
     wm_class = client.window.get_wm_class()[0]

     for i in range(len(d)):
         if wm_class in list(d.values())[i]:
             group = list(d.keys())[i]
             client.togroup(group)
             client.group.cmd_toscreen(toggle=False)

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME



main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='confirmreset'),
    Match(wm_class='makebranch'),
    Match(wm_class='maketag'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),
    Match(title='branchdialog'),
    Match(title='Open File'),
    Match(title='pinentry'),
    Match(wm_class='ssh-askpass'),
    Match(wm_class='VirtualBox Manager'),

],  fullscreen_border_width = 0, border_width = 0)
auto_fullscreen = True

focus_on_window_activation = "focus" # or smart

wmname = "qtile"
