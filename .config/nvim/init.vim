" init.vim
" v. 0.4
" by chris noseworthy
" https://gitlab.com/cnose
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible
filetype plugin on
syntax on
set termguicolors
let mapleader=" "

call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'vim-python/python-syntax'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-commentary'
Plug 'mcchrish/nnn.vim'
Plug 'sheerun/vim-polyglot'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'reedes/vim-pencil'
Plug 'voldikss/vim-floaterm'
Plug 'machakann/vim-highlightedyank'
Plug 'machakann/vim-sandwich'
call plug#end()

lua require'colorizer'.setup()

" FloaTerm options
let g:floaterm_keymap_new = '<Leader>t'
let g:floaterm_title = 'terminal($1|$2)'
let g:floaterm_position = 'right'

" Goyo config
let g:goyo_width = 160
nnoremap <leader>g :Goyo<cr>

" limelight
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" nnn configs
let g:nnn#layout = { 'window': { 'width': 0.4, 'height': 0.8, 'highlight': 'Debug', 'xoffset': 1 } }
let g:nnn#action = {
      \ '<leader>t': 'tab split',
      \ '<leader>s': 'split',
      \ '<leader>v': 'vsplit' }
let g:nnn#command = 'nnn -DR'

" vim markdown configs
let g:vim_markdown_folding_disabled = 1
set conceallevel =2

" pencil config
nnoremap <leader>p :Pencil<cr>
augroup pencil
    autocmd!
    autocmd FileType markdown,mkd call pencil#init({'wrap': 'soft'})
    autocmd FileType text         call pencil#init({'wrap': 'soft'})
augroup END

" Use terminal transparent background (disabling termguicolors also removes
" the background)
" augroup nord-theme-overrides
"     autocmd!
"     autocmd Colorscheme nord highlight Normal guibg=none
" augroup END

"  Mouse Scrolling
set mouse=nicr

"  copy/paste between vim and other programs
set clipboard=unnamedplus

"  lightline
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ } 

" Always show statusline
set laststatus=2

" relative line numbering
set number relativenumber

let g:rehash256 = 1

" prevent non-normal modes showing in powerline and below powerline.
set noshowmode

" to keep multiple buffers open
set hidden

" Save 1,000 items in history
set history=1000

" Show the line and column number of the cursor position
set ruler

" Display the incomplete commands in the bottom right-hand side of your screen.  
set showcmd

" Display completion matches on your status line
set wildmenu

" Show a few lines of context around the cursor
set scrolloff=5

" search and case.
set nohlsearch
set incsearch
set ignorecase
set smartcase

" Turn off file backups and swap
set nobackup
set noswapfile

" Lines, indents, spaces and tabs.
set lbr
set nowrap
set autoindent
set smartindent
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" Tell vim what background you are using
" set bg=light
set bg=dark

" set colorscheme and options
let g:nord_cursor_line_number_background = 1
let g:nord_bold_vertical_split_line = 1
colorscheme nord

" Quickly save, quit, save&quit your file.
map <leader>w :w!<cr>
map <leader>x :q!<cr>
map <leader>q :wq<cr>

" Splits and Tabbed Files
set splitbelow splitright

" splits & tabs navigation
nnoremap <C-h> :wincmd h<CR>
nnoremap <C-j> :wincmd j<CR>
nnoremap <C-k> :wincmd k<CR>
nnoremap <C-l> :wincmd l<CR>
nnoremap <C-x> :wincmd c<CR>
nnoremap <C-p> :tabp<CR>
nnoremap <C-n> :tabn<CR>

" splits resizing
nnoremap <silent> <C-Left> :vertical resize +5<CR>
nnoremap <silent> <C-Right>  :vertical resize -5<CR>
nnoremap <silent> <C-Up> :resize +5<CR>
nnoremap <silent> <C-Down> :resize -5<CR>

" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

" other
"set fillchars+=vert:\ 
let g:python_highlight_all = 1
nnoremap <leader><CR> :so ~/.config/nvim/init.vim<CR>
