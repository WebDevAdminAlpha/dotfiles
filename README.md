# dotfiles for [qtile](http://www.qtile.org/)

A collection of some of my dotfiles/scripts for my ArcoLinux Qtile installation.

![cap01](.config/qtile/cap01.png)  
![cap02](.config/qtile/cap02.png)  
![cap03](.config/qtile/cap03.png)

I have two ArcoLinux installations; one on a laptop (daily driver) and the other is a desktop installation (mostly a Plex/torrenting server).

I use termite as my terminal, and I have tried to theme my entire installation with the Nord theme, with varying degrees of success.

I have added several terminal-based applications and try to do as much as I can in the terminal. I have borrowed and modified scripts from both [distrotube](https://gitlab.com/dwt1) and [Luke Smith](https://gitlab.com/LukeSmithxyz) in order to learn about shell scripting. 

Feel free to borrow/steal anything from this repo (unless I accidentally put personal info up here, then plz don't).
